using Toybox.WatchUi;

//any functions (and global vars) to be used from delegates go here
var txt = "Testing";

var txtColor = null; //Graphics.COLOR_BLACK;
var txtBGColor = null; //Graphics.COLOR_TRANSPARENT;
var screenColor = null; //Graphics.COLOR_GREEN;

function setText(inputText,txtcol){
	txt = inputText;
	txtColor = txtcol==null ? txtColor : txtcol;
	WatchUi.requestUpdate();
}

class trackerView extends WatchUi.View {

    function initialize() {
        View.initialize();
        txtColor = Graphics.COLOR_BLACK;
		txtBGColor = Graphics.COLOR_TRANSPARENT;
		screenColor = Graphics.COLOR_GREEN;
    }

    // Load your resources here
    function onLayout(dc) {
        setLayout(Rez.Layouts.MainLayout(dc));
    }

    // Called when this View is brought to the foreground. Restore
    // the state of this View and prepare it to be shown. This includes
    // loading resources into memory.
    function onShow() {
    }

    // Update the view
    function onUpdate(dc) {
        // Call the parent onUpdate function to redraw the layout
          //View.onUpdate(dc);
		displayText(dc, txt, txtColor, txtBGColor, screenColor);
    }

    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() {
    }

    
    function displayText(dc2, inputText, txtColor, bgColor, screenColor){
    	// set up background color by using 2nd argument here and then calling clear(). 
		dc2.setColor(
			Graphics.COLOR_BLACK, 
			screenColor == null ? Graphics.COLOR_GREEN : screenColor);
		dc2.clear();
		//now background has been drawn, set text color
		dc2.setColor(
			txtColor == null ? Graphics.COLOR_BLACK : txtColor, 
			bgColor == null ? Graphics.COLOR_TRANSPARENT : bgColor
		);
		//now draw text
		dc2.drawText(120,120, Graphics.FONT_MEDIUM, inputText, Graphics.TEXT_JUSTIFY_CENTER);
    }

}
