using Toybox.WatchUi;
using Toybox.Graphics;
using Toybox.System;

class trackerDelegate extends WatchUi.BehaviorDelegate {

    enum {
        ON_NEXT_PAGE,
        ON_PREV_PAGE,
        ON_MENU,
        ON_BACK,
        ON_NEXT_MODE,
        ON_PREV_MODE,
        ON_SELECT
    }

	var last_key = null;
	var last_behavior = null;
	var buttons_pressed = null;
	var buttons_expected = null;
	var last_tap = -1;
	
    function initialize() {
        BehaviorDelegate.initialize();

        buttons_pressed = 0;

        var deviceSettings = System.getDeviceSettings();
        buttons_expected = deviceSettings.inputButtons;
    }
    
    function onTap(evt) {
	    	//setText("Testing",Graphics.COLOR_RED);
	    	WatchUi.pushView(new trackermenuView(), new trackermenuDelegate(), WatchUi.SLIDE_UP);
	    	

        return true;
    }    


}