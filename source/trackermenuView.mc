using Toybox.WatchUi;

//any functions (and global vars) to be used from delegates go here
var othertxt = "Testing";


//function setText(inputText,txtcol){
//	txt = inputText;
//	txtColor = txtcol==null ? txtColor : txtcol;
//	WatchUi.requestUpdate();
//}

class trackermenuView extends WatchUi.View {

    function initialize() {
        View.initialize();
    }

    // Load your resources here
    function onLayout(dc) {
        setLayout(Rez.Layouts.MainLayout(dc));
    }

    // Called when this View is brought to the foreground. Restore
    // the state of this View and prepare it to be shown. This includes
    // loading resources into memory.
    function onShow() {
    }

    // Update the view
    function onUpdate(dc) {
        // Call the parent onUpdate function to redraw the layout
          //View.onUpdate(dc);
		displayMenu(dc);
    }

    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() {
    }

    
    function displayMenu(dc2){
    	// set up background color by using 2nd argument here and then calling clear(). 
		dc2.setColor(Graphics.COLOR_BLACK,Graphics.COLOR_WHITE);
		dc2.clear();
		//now background has been drawn, set text color
		dc2.setColor(Graphics.COLOR_BLACK,Graphics.COLOR_TRANSPARENT);
		//now draw text
		dc2.drawText(120,120, Graphics.FONT_MEDIUM, "menu", Graphics.TEXT_JUSTIFY_CENTER);
    }

}
